#ifndef NIXIE_DISPLAY_APP_HPP
#define NIXIE_DISPLAY_APP_HPP

#include <xpcc/architecture/platform.hpp>
using namespace xpcc::atmega;

namespace Nixie
{
	typedef GpioOutputC2 Cathode0;
	typedef GpioOutputC0 Cathode1;
	typedef GpioOutputC1 Cathode2;
	typedef GpioOutputA6 Cathode3;
	typedef GpioOutputC5 Cathode4;
	typedef GpioOutputA5 Cathode5;
	typedef GpioOutputA7 Cathode6;
	typedef GpioOutputD7 Cathode7;
	typedef GpioOutputC4 Cathode8;
	typedef GpioOutputC3 Cathode9;

	typedef GpioOutputD4 Anode1;
	typedef GpioOutputD3 Anode2;
	typedef GpioOutputD2 Anode3;
	typedef GpioOutputD1 Anode4;
	typedef GpioOutputD5 Anode5;
	typedef GpioOutputD6 Anode6;	

	typedef GpioOutputD0 Dots;

	typedef GpioInputB0 ButtonInput;
}

#endif