#ifndef IRRMNTSAMLLCHINA00
#define IRRMNTSAMLLCHINA00

#include "IRremote.hpp"

//          77         213         151   --> Code
//      On/Off      Source  Speker Off   --> Key

//         205          x5         207
//      Record         CH+   TimeShift

//         175         253         135
//        VOl- Full Screen        Vol+

//         1x3         1x1         1xx
//           0         CH-      Recall

//         111          71           7
//           1           2           3

//          7x         103          3x
//           4           5           6

//         11x          x7          23
//           7           8           9

class IRrmntSmallChina00: public IRrecv
{
	private:
		decode_results results;
	protected:
	public:
		enum KeyCodes
		{
			KeyCode_Unknown     =   0,
			KeyCode_None        =   1,
			KeyCode_OnOff       =  77,
			KeyCode_Source      = 213,
			KeyCode_SpeakerOff  = 151,
			KeyCode_Record      = 205,
			KeyCode_CHPlus      =  95,
			KeyCode_TimeShift 	= 207,
			KeyCode_VolMinus    = 175,
			KeyCode_FullScreen  = 253,
			KeyCode_VolPlus     = 135,
			KeyCode_CHMinus     = 191,
			KeyCode_Recall      = 188,
			KeyCode_Button0     = 183,
			KeyCode_Button1     = 111,
			KeyCode_Button2     =  71,
			KeyCode_Button3     =   7,
			KeyCode_Button4     =  78,
			KeyCode_Button5     = 103,
			KeyCode_Button6     =  38,
			KeyCode_Button7     = 118,
			KeyCode_Button8     =   8,
			KeyCode_Button9     =  23,
			KeyCode_Repeat      = 255,	
		};

		KeyCodes keyCode;
		KeyCodes Decode();
		KeyCodes GetKeyCode();
};

#endif