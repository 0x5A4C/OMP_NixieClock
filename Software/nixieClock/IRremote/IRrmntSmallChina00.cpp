#include "IRrmntSmallChina00.hpp"

IRrmntSmallChina00::KeyCodes IRrmntSmallChina00::Decode()
{
	if(this->decode(&(this->results)))
	  {
//	     if((this->results.decodedValue.value & 0xffff) != 0xffff)
	     {

	        this->keyCode = (IRrmntSmallChina00::KeyCodes)results.decodedValue.necCommand.command;
	     }

	     this->resume();
	  }
	else
	{
		this->keyCode = IRrmntSmallChina00::KeyCode_None;
	}

	return this->keyCode;
}

IRrmntSmallChina00::KeyCodes IRrmntSmallChina00::GetKeyCode()
{
	IRrmntSmallChina00::KeyCodes keyCode;

	keyCode = this->keyCode;
	this->keyCode = IRrmntSmallChina00::KeyCode_None;
	return keyCode;
}
