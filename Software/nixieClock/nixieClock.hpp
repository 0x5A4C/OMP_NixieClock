#ifndef NIXIE_CLOCK_HPP
#define NIXIE_CLOCK_HPP

#include "nixieDisplayApl.hpp"
#include "nixieDisplay.hpp"
#include "Button.hpp"
#include "IRremote/IRrmntSmallChina00.hpp"

#include <xpcc/architecture/platform.hpp>

#include <xpcc/architecture/interface/gpio.hpp>
#include <xpcc/ui/button_group.hpp>

#include <xpcc/processing/protothread.hpp>
#include <xpcc/processing/timer.hpp>

#include "Fsm/FiniteStateMachine.h"

	typedef GpioInputB0 ButtonInput;
	enum
	{
		BUTTON_1 = 0x01
	};
	static xpcc::ButtonGroup<> buttons(BUTTON_1);

namespace Nixie
{
	//Process
	//buttons.update(Button::read() ? BUTTON_1 : 0);
	//Setup
	//Button::setInput(Gpio::InputType::PullUp);
	//App
	//buttons.isPressed(BUTTON_1)
	// typedef GpioInputB0 ButtonInput;
	// enum
	// {
	// 	BUTTON_1 = 0x01
	// };
	// static xpcc::ButtonGroup<> buttons(BUTTON_1);

	// class Button: public xpcc::pt::Protothread, private xpcc::co::NestedCoroutine<2>
	// {
	// private:
	// 	xpcc::ShortTimeout timeout;
	// public:
	// 	Button(void)
	// 	{
	// 		ButtonInput::setInput(Gpio::InputType::PullUp);
	// 		ButtonInput::setInput();
	// 	}

	// 	xpcc::co::Result<bool> Process(void)
	// 	{
	// 		CO_BEGIN();
	// 		this->timeout.restart(10);
	// 		CO_WAIT_UNTIL(this->timeout.isExpired());
	// 		buttons.update(ButtonInput::read() ? BUTTON_1 : 0);
	// 		CO_END_RETURN(true);
	// 	}

	// 	bool IsPressed(void)
	// 	{
	// 		return buttons.isPressed(BUTTON_1);
	// 	}

	// 	bool IsReleased(void)
	// 	{
	// 		return buttons.isReleased(BUTTON_1);
	// 	}

	// 	bool IsRepeated(void)
	// 	{
	// 		return buttons.isRepeated(BUTTON_1);
	// 	}

	// 	bool IsPressedLong(void)
	// 	{
	// 		return buttons.isPressedLong(BUTTON_1);
	// 	}		
	// };

	class GreenLed: GpioOutputB1
	{
	public:
		GreenLed(void);
		void On(void);
		void Off(void);
		void Toggle(void);
	};

	class Clock : public xpcc::pt::Protothread, private xpcc::co::NestedCoroutine<2>
	{
	private:
		xpcc::ShortTimeout timeout;

	protected:

	public:
		Display display;
		GreenLed greenLed;
		Nixie::Button button;
		IRrmntSmallChina00 iRrmntSmallChina00;

		Clock(void);

		bool Run(void);
		void Test(void);

		void SetTime_Enter(void);
		void SetTime_Update(void);
		void SetTime_Exit(void);		
	};	
}

#endif