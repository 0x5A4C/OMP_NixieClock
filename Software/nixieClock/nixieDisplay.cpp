#include "nixieDisplay.hpp"

//--Cathodes-----------------------------------------------------------------------------------------------------
void Nixie::Cathodes::Init(void)
{
   Nixie::Cathode0::setOutput();
   Nixie::Cathode1::setOutput();
   Nixie::Cathode2::setOutput();
   Nixie::Cathode3::setOutput();
   Nixie::Cathode4::setOutput();
   Nixie::Cathode5::setOutput();
   Nixie::Cathode6::setOutput();
   Nixie::Cathode7::setOutput();
   Nixie::Cathode8::setOutput();
   Nixie::Cathode9::setOutput();   
}

void Nixie::Cathodes::Set(uint8_t cathodeIndex)
{
   switch(cathodeIndex)
   {
      case 0x00:
         Cathode0::set();
         break;
      case 0x01:
         Cathode1::set();
         break;
      case 0x02:
         Cathode2::set();
         break;
      case 0x03:
         Cathode3::set();
         break;
      case 0x04:
         Cathode4::set();
         break;
      case 0x05:
         Cathode5::set();
         break;
      case 0x06:
         Cathode6::set();
         break;
      case 0x07:
         Cathode7::set();
         break;
      case 0x08:
         Cathode8::set();
         break;
      case 0x09:
         Cathode9::set();
         break;         
      default:
         break;
   }
}

void Nixie::Cathodes::Reset(uint8_t cathodeIndex)
{
   switch(cathodeIndex)
   {
      case 0x00:
         Cathode0::reset();
         break;
      case 0x01:
         Cathode1::reset();
         break;
      case 0x02:
         Cathode2::reset();
         break;
      case 0x03:
         Cathode3::reset();
         break;
      case 0x04:
         Cathode4::reset();
         break;
      case 0x05:
         Cathode5::reset();
         break;
      case 0x06:
         Cathode6::reset();
         break;
      case 0x07:
         Cathode7::reset();
         break;
      case 0x08:
         Cathode8::reset();
         break;
      case 0x09:
         Cathode9::reset();
         break;         
      default:
         break;
   }
}

void Nixie::Cathodes::Toggle(uint8_t cathodeIndex)
{
   switch(cathodeIndex)
   {
      case 0x00:
         Cathode0::toggle();
         break;
      case 0x01:
         Cathode1::toggle();
         break;
      case 0x02:
         Cathode2::toggle();
         break;
      case 0x03:
         Cathode3::toggle();
         break;
      case 0x04:
         Cathode4::toggle();
         break;
      case 0x05:
         Cathode5::toggle();
         break;
      case 0x06:
         Cathode6::toggle();
         break;
      case 0x07:
         Cathode7::toggle();
         break;
      case 0x08:
         Cathode8::toggle();
         break;
      case 0x09:
         Cathode9::toggle();
         break;
      default:
         break;
   }
}

void Nixie::Cathodes::SetAll(void)
{
   Cathode0::set();
   Cathode1::set();
   Cathode2::set();
   Cathode3::set();
   Cathode4::set();
   Cathode5::set();
   Cathode6::set();
   Cathode7::set();
   Cathode8::set();
   Cathode9::set();   
}

void Nixie::Cathodes::ResetAll(void)
{
   Cathode0::reset();
   Cathode1::reset();
   Cathode2::reset();
   Cathode3::reset();
   Cathode4::reset();
   Cathode5::reset();
   Cathode6::reset();
   Cathode7::reset();
   Cathode8::reset();
   Cathode9::reset();
}

void Nixie::Cathodes::ToggleAll(void)
{
   Cathode0::toggle();
   Cathode1::toggle();
   Cathode2::toggle();
   Cathode3::toggle();
   Cathode4::toggle();
   Cathode5::toggle();
   Cathode6::toggle();
   Cathode7::toggle();
   Cathode8::toggle();
   Cathode9::toggle();
}

//--Lamp---------------------------------------------------------------------------------------------------------
template <typename LampAnode>
Nixie::Lamp<LampAnode>::Lamp(void)
{
   LampAnode::setOutput();
}

template <typename LampAnode>
void Nixie::Lamp<LampAnode>::Activate(void)
{
   this->counter--;
   if(!blink)
   {
      LampAnode::set();
   }
   else
   {
      if(this->counter > (BLINKCOUNTER/3) )
      {
         LampAnode::set();
      }
      else
      {
         LampAnode::reset();      
      }

      if(!this->counter)
      {
         this->counter = BLINKCOUNTER;
      }      
   }
}

template <typename LampAnode>
void Nixie::Lamp<LampAnode>::Hide(void)
{
   LampAnode::reset();
}

template <typename LampAnode>
void Nixie::Lamp<LampAnode>::Set(uint8_t digitToActivate)
{
   Nixie::Cathodes::ResetAll();
   Nixie::Cathodes::Set(digitToActivate);
}

template <typename LampAnode>
void Nixie::Lamp<LampAnode>::Blink(uint8_t blinkCounter)
{
   if(blinkCounter)
   {
   this->blink = true;
   }
   else
   {
   this->blink = false;
   }

   this->counter = blinkCounter;   
}


template <typename LampAnode>
void Nixie::Lamp<LampAnode>::Process(uint8_t digitToActivate)
{
   this->Hide();
   this->Set(digitToActivate);
   this->Activate();
}

//--Display------------------------------------------------------------------------------------------------------
Nixie::Display::Display(void)
{
   Nixie::Cathodes::Init();
   displayMem.digits.d1 = 0x01;
   displayMem.digits.d2 = 0x02;
   displayMem.digits.d3 = 0x03;
   displayMem.digits.d4 = 0x04;
   displayMem.digits.d5 = 0x05;
   displayMem.digits.d6 = 0x06;   
}

void Nixie::Display::Process(void)
{
      static uint8_t digitNumber = 0x00;

      SetLamp(digitNumber, displayMem.mem[digitNumber]);

      digitNumber += 1;
      if(digitNumber > 5)
         digitNumber = 0x00;     
}

void Nixie::Display::SetLamp(uint8_t lampIndex, uint8_t digitToActivate)
{
   Nixie::Cathodes::ResetAll();
   L0.Hide();
   L1.Hide();
   L2.Hide();
   L3.Hide();
   L4.Hide();
   L5.Hide();

   switch(lampIndex)
   {
      case 0:
         L0.Set(digitToActivate);
         L0.Activate();
         break;
      case 1:
         L1.Set(digitToActivate);
         L1.Activate();         
         break;
      case 2:
         L2.Set(digitToActivate);
         L2.Activate();
         break;
      case 3:
         L3.Set(digitToActivate);
         L3.Activate();
         break;
      case 4:
         L4.Set(digitToActivate);
         L4.Activate();         
         break;
      case 5:
         L5.Set(digitToActivate);
         L5.Activate();         
         break;         
      default:
         break;
   }
}

void Nixie::Display::DisplayData(uint8_t dataLeft, uint8_t dataMidle, uint8_t dataRight)
{
   displayMem.digits.d1 = dataLeft / 10;
   displayMem.digits.d2 = dataLeft % 10;
   displayMem.digits.d3 = dataMidle / 10;
   displayMem.digits.d4 = dataMidle % 10;
   displayMem.digits.d5 = dataRight / 10;
   displayMem.digits.d6 = dataRight % 10; 
}

void Nixie::Display::DisplayDataBCD(uint8_t dataLeft, uint8_t dataMidle, uint8_t dataRight)
{
   displayMem.digits.d1 = dataLeft >> 4;
   displayMem.digits.d2 = dataLeft & 0x0F;
   displayMem.digits.d3 = dataMidle >> 4;
   displayMem.digits.d4 = dataMidle & 0x0F;
   displayMem.digits.d5 = dataRight >> 4;
   displayMem.digits.d6 = dataRight & 0x0F; 
}

void Nixie::Display::DisplaySeparateDigits(uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5)
{
   displayMem.digits.d1 = d0;
   displayMem.digits.d2 = d1;
   displayMem.digits.d3 = d2;
   displayMem.digits.d4 = d3;
   displayMem.digits.d5 = d4;
   displayMem.digits.d6 = d5; 
}

void Nixie::Display::Blink(bool blinkFlag)
{
   uint8_t blinkCtr;

   blinkCtr = blinkFlag ? BLINKCOUNTER : 0x00;
   L0.Blink(blinkCtr);
   L1.Blink(blinkCtr);
   L2.Blink(blinkCtr);
   L3.Blink(blinkCtr);
   L4.Blink(blinkCtr);
   L5.Blink(blinkCtr);
}

void Nixie::Display::SetDigitIntoSetup(uint8_t digitIndex)
{
   switch(digitIndex)
   {
      case 0x00:
         L0.Blink(0x00);
         break;
      case 0x01:
         L1.Blink(0x00);
         break;
      case 0x02:
         L2.Blink(0x00);
         break;
      case 0x03:
         L3.Blink(0x00);
         break;
      case 0x04:
         L4.Blink(0x00);
         break;
      case 0x05:
         L5.Blink(0x00);
         break;         
      default:
         break;
   }
}

void Nixie::Display::SetDigitIntoNormal(uint8_t digitIndex)
{
   switch(digitIndex)
   {
      case 0x00:
         L0.Blink(BLINKCOUNTER);
         break;
      case 0x01:
         L1.Blink(BLINKCOUNTER);
         break;
      case 0x02:
         L2.Blink(BLINKCOUNTER);
         break;
      case 0x03:
         L3.Blink(BLINKCOUNTER);
         break;
      case 0x04:
         L4.Blink(BLINKCOUNTER);
         break;
      case 0x05:
         L5.Blink(BLINKCOUNTER);
         break;         
      default:
         break;
   }   
}

void Nixie::Display::SetDisplayIntoNormal(void)
{
   L0.Blink(BLINKCOUNTER);
   L1.Blink(BLINKCOUNTER);
   L2.Blink(BLINKCOUNTER);
   L3.Blink(BLINKCOUNTER);
   L4.Blink(BLINKCOUNTER);
   L5.Blink(BLINKCOUNTER);
}

void Nixie::Display::Test(void)
{
}
