#ifndef NIXIE_DISPLAY_HPP
#define NIXIE_DISPLAY_HPP

#include "nixieDisplayApl.hpp"

namespace Nixie
{
	class Cathodes
	{
	private:
	protected:
	public:
		static void Init(void);
		static void Set(uint8_t cathodeIndex);
		static void Reset(uint8_t cathodeIndex);
		static void Toggle(uint8_t cathodeIndex);
		static void SetAll(void);
		static void ResetAll(void);
		static void ToggleAll(void);		
	};


	#define BLINKCOUNTER (50)

	template <typename LampAnode>
	class Lamp
	{
	private:
		uint8_t counter = BLINKCOUNTER;
		bool blink = false;
	protected:
	public:
		Lamp(void);
		void Activate(void);
		void Hide(void);
		void Set(uint8_t digitToActivate);
		void Blink(uint8_t blinkCounter);
		void Process(uint8_t digitToActivate);
	};

	typedef Lamp<Anode1> Lamp0;
	typedef Lamp<Anode2> Lamp1;
	typedef Lamp<Anode3> Lamp2;
	typedef Lamp<Anode4> Lamp3;
	typedef Lamp<Anode5> Lamp4;
	typedef Lamp<Anode6> Lamp5;

	class Display
	{
	private:

	protected:

	public:
		struct DigitsMem
		{
			uint8_t d1;
			uint8_t d2;
			uint8_t d3;
			uint8_t d4;
			uint8_t d5;
			uint8_t d6;
		};

		union DisplayMem
		{
			struct  DigitsMem digits;
			uint8_t mem[6];
		} displayMem;

		Lamp<Anode1> L0;
		Lamp<Anode2> L1;
		Lamp<Anode3> L2;
		Lamp<Anode4> L3;
		Lamp<Anode5> L4;
		Lamp<Anode6> L5;

		// Dot	 Dt;

		Display(void);
		void Process(void);
		void Test(void);
		void SetLamp(uint8_t lampIndex, uint8_t digitToActivate);
		void DisplayData(uint8_t dataLeft, uint8_t dataMidle, uint8_t dataRight);
		void DisplayDataBCD(uint8_t dataLeft, uint8_t dataMidle, uint8_t dataRight);
		void DisplaySeparateDigits(uint8_t d0, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4, uint8_t d5);
 		void Blink(bool blinkFlag);
		void SetDigitIntoSetup(uint8_t digitIndex);
		void SetDigitIntoNormal(uint8_t digitIndex);
		void SetDisplayIntoNormal(void);
	};
}
#endif