#include <xpcc/architecture/platform.hpp>
#include <xpcc/processing/protothread.hpp>
#include <xpcc/processing/timer.hpp>

#include "nixieClock.hpp"
#include <xpcc/architecture/interface/gpio.hpp>

#include <xpcc/architecture.hpp>

#include "nixieDcf.hpp"

using namespace xpcc::atmega;

typedef xpcc::avr::SystemClock systemClock;

extern Nixie::Clock nixieClock;

//------------------------------------------------------------

inline void Timer2Setup_Ovf(void)
{
	// timeout initialization
	// overflow-interrupt every 1 ms at 16 MHz
	// set up timer with prescaler = 64
	TCCR2 |= (1 << CS22);
	// initialize counter
	//TCNT2 = 131;
	TCNT2 = 4;
	// enable overflow interrupt
	TIMSK |= (1 << TOIE2);
}

ISR(TIMER2_OVF_vect)
{
	TCNT2 = 4;

	xpcc::Clock::increment();
  	nixieClock.display.Process();
}

inline void Timer2Setup_Cmp(void)
{
	// timeout initialization
	// compare-interrupt every 1 ms at 16 MHz
	// set up timer with prescaler = 8
	TCCR2 |= (1 << CS21);
	// initialize counter
	TCNT2 = 0xff - 195;
	// initialize overflow count register	
	OCR2 = 0xff - 7;
	// enable compare interrupt
	TIMSK |= (1 << OCIE2);
}

ISR(TIMER2_COMP_vect)
{
	TCNT2 = 0xff - 195;

	xpcc::Clock::increment();
	nixieClock.iRrmntSmallChina00.Decode();
  	nixieClock.display.Process();
}

/*----------------------------------------------------------------------------------------*/
typedef GpioB0 SignalInput;
DcfSignalReceiver<SignalInput> dcfSignalReceiver;

xpcc::ShortTimeout timeoutForStartBit;
static xpcc::Timestamp startingEdgeTimestamp( xpcc::Clock::now() );
int main()
{
	systemClock::enable();

	Timer2Setup_Ovf();

	//DcfReceiver::Init();

	enableInterrupts();

	timeoutForStartBit.restart(10);

	while (1)
	{
		dcfSignalReceiver.Process();

		// if(timeoutForStartBit.isExpired())
		// {
		// 	if(dcfDateTime.dcfSignalReceiver.signalEdge == SignalEdge_Unknown)
		// 	{
		// 		nixieClock.display.DisplayDataBCD(0, 0, 0x00);
		// 	}
		// 	if(dcfDateTime.dcfSignalReceiver.signalEdge == SignalEdge_Rising)
		// 	{
		// 		timeoutForStartBit.restart(100);
		// 		nixieClock.display.DisplayDataBCD(0, 0, 0x10);
		// 	}
		// 	if(dcfDateTime.dcfSignalReceiver.signalEdge == SignalEdge_Falling)
		// 	{
		// 		timeoutForStartBit.restart(100);
		// 		nixieClock.display.DisplayDataBCD(0, 0, 0x01);
		// 	}
		// }

		// if(dcfDateTime.dcfSignalReceiver.startBit == SignalValue_High)
		// {
		// 	nixieClock.display.DisplayDataBCD(0x01, 0x01, 0x01);
		// 	timeoutForStartBit.restart(5000);
		// }
		// else
		// {
		// 	if(timeoutForStartBit.isExpired())
		// 	{
		// 		if(dcfDateTime.dcfSignalReceiver.signalEdge == SignalEdge_Unknown)
		// 		{
		// 			nixieClock.display.DisplayDataBCD(0, 0, 0x00);
		// 		}
		// 		if(dcfDateTime.dcfSignalReceiver.signalEdge == SignalEdge_Rising)
		// 		{
		// 			timeoutForStartBit.restart(100);
		// 			nixieClock.display.DisplayDataBCD(0, 0, 0x10);
		// 		}
		// 		if(dcfDateTime.dcfSignalReceiver.signalEdge == SignalEdge_Falling)
		// 		{
		// 			timeoutForStartBit.restart(100);
		// 			nixieClock.display.DisplayDataBCD(0, 0, 0x01);
		// 		}
		// 	}
		// }
/*
		if(dcfDateTime.dcfSignalReceiver.startBit == SignalValue_High)
		{
			nixieClock.display.DisplayDataBCD(0x01, 0x01, 0x01);
			timeoutForStartBit.restart(500);
		}
		else
		{
			if(timeoutForStartBit.isExpired())
			{
				if(dcfDateTime.dcfSignalReceiver.regularBitValue == SignalValue_Unknown)
				{
					nixieClock.display.DisplayDataBCD(0, 0, 0x00);
				}				
				if(dcfDateTime.dcfSignalReceiver.regularBitValue == SignalValue_High)
				{
					timeoutForStartBit.restart(100);					
					nixieClock.display.DisplayDataBCD(0, 0, 0x10);
				}
				if(dcfDateTime.dcfSignalReceiver.regularBitValue == SignalValue_Low)
				{
					timeoutForStartBit.restart(100);					
					nixieClock.display.DisplayDataBCD(0, 0, 0x01);
				}
			}
		}
*/
		nixieClock.display.DisplayData(dcfSignalReceiver.hour, dcfSignalReceiver.minute, dcfSignalReceiver.second);

		// DcfReceiver::Process();

		// if(DcfReceiver::StartBit == 1)
		// {
		// 	nixieClock.display.DisplayDataBCD(1, 1, 1);
		// 	timeoutForStartBit.restart(5000);
		// }
		// else
		// {
		// 	if(timeoutForStartBit.isExpired())
		// 	{
		// 		nixieClock.display.DisplayData(DcfTime::getHours(), DcfTime::getMinutes(), DcfTime::getSeconds());
		// 	}
		// }
	}
}
