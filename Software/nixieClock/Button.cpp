#include "Button.hpp"
#include "ButtonApp.h"

#include <cstddef>

void (*Button_StateChanged) (Nixie::ButtonState buttonState) = NULL;

// typedef GpioInputB3 Button;
// Button::read()

//template<typename T>
void Nixie::Button::ButtonDebounce()
{
//	sButton *button = Buttons[buttonIndex];
	uint8_t event = false;

	if(ButtonInput::read() != 0)
	{
		if (this->integrator > 0)
			this->integrator--;
	}
	else
	{
		if (this->integrator < MAXIMUM)
			this->integrator++;
		if (this->integratorLongPress < MAXIMUM_LONG_P)
			this->integratorLongPress++;
	}

	/* Step 2: Update the output state based on the integrator.  Note that the
	 output will only change states if the integrator has reached a limit, either
	 0 or MAXIMUM. */

	if(this->integrator == 0)
	{
		this->integratorLongPress = 0x00;
		this->state = eButtonStateReleased;
		event = true;
	}
	else
	{
		if(this->integrator >= MAXIMUM)
		{
			this->state = eButtonStatePresed;
			this->integrator = MAXIMUM; /* defensive code if integrator got corrupted */
			event = true;
		}
		if(this->integratorLongPress >= MAXIMUM_LONG_P)
		{
			this->state = eButtonStateLongPressed;
			this->integratorLongPress = MAXIMUM_LONG_P;
			event = true;
		}
	}

	if(event == true)
	{
		if(this->state != this->stateOld)
		{
			this->stateOld = this->state;
			this->stateEv = this->state;
			if(Button_StateChanged != NULL)
				Button_StateChanged(this->state);

			if((this->state == eButtonStatePresed) /*&& (this->stateOld == eButtonStateReleased)*/)
			{
				if(this->doublePressedTimeout.remaining() > 0)
				{
					this->counterPressed++;
				}
				this->doublePressedTimeout.restart(DOUBLECLICKTIMEOUT);
			}
		}
	}

	if(this->doublePressedTimeout.isExpired())
	{
		this->counterPressed = 0x00;
	}
}

//template<typename T>
void
Nixie::Button::ButtonProcess()
{
	this->ButtonDebounce();
}

//template<typename T>
Nixie::ButtonState
Nixie::Button::ButtonGetState()
{
	ButtonState state = this->stateEv;
	this->stateEv = eButtonStateNone;
	return state;
}


#if 0
void Nixie::Button::ButtonSingleInit(sButtonPinPort buttonPinPort)
{
#ifdef __AVR__
#if defined (__AVR_ATmega16__)
	if(buttonPinPort.port == &PINA)
	{
		CLEARBIT(DDRA, buttonPinPort.pin);
		SETBIT(PORTA, buttonPinPort.pin);
	}
#else
   if(0)
   {
   }
#endif
	else if(buttonPinPort.port == &PINB)
	{
		CLEARBIT(DDRB, buttonPinPort.pin);
		SETBIT(PORTB, buttonPinPort.pin);
	}
	else if(buttonPinPort.port == &PINC)
	{
		CLEARBIT(DDRC, buttonPinPort.pin);
		SETBIT(PORTC, buttonPinPort.pin);
	}
	else if(buttonPinPort.port == &PIND)
	{
		CLEARBIT(DDRD, buttonPinPort.pin);
		SETBIT(PORTD, buttonPinPort.pin);
	}
#endif
}
#endif

// void ButtonInit(void (*pButton_StateChanged) (uint8_t buttonNumber, Nixie::Button::ButtonState buttonState))
// {
// #if 0
// 	Button_StateChanged = pButton_StateChanged;
// 	for (uint8_t index = 0x00; index < COUNT_OF(Buttons); index++)
// 	{
// #ifdef __AVR__
// 		Button_SingleInit(Buttons[index]->pinPort);
// #endif
// 	}
// #endif
// }
