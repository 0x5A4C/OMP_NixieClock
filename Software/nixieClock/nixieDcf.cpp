#include <xpcc/processing/timer.hpp>
#include "nixieDcf.hpp"

#include <avr/io.h>

#define BIT_0        (0x00)
#define BIT_1        (0x01)
#define BIT_UNKNOWN  (0xff)

#define EDGE_RISING  (0x01)
#define EDGE_FALLING (0x00)
#define EDGE_UNKNOWN (0xff)

/*----------------------------------------------------------------------------------------------------*/
// bool DcfReceiver::edgeDetected = false;
// uint8_t DcfReceiver::StartBit  = BIT_UNKNOWN;
// uint8_t DcfReceiver::CurentBit = BIT_UNKNOWN;;

// uint8_t bufferBit[60];
// uint8_t bufferBitIndex = 0x00;

// void DcfTime::resetBufferBitIndex(void)
// {
//     bufferBitIndex = 0x00;
// }

// void DcfTime::addBit(uint8_t bit)
// {
//     if(bufferBitIndex < 59)
//     {
//         bufferBit[bufferBitIndex] = bit;
//         bufferBitIndex += 1;
//     }
// }

// uint8_t DcfTime::getSeconds(void)
// {
//     return bufferBitIndex;
// }

// uint8_t DcfTime::getMinutes(void)
// {
//     return  (   (bufferBit[21] *  1)+
//                 (bufferBit[22] *  2)+
//                 (bufferBit[23] *  4)+
//                 (bufferBit[24] *  8)+
//                 (bufferBit[25] * 10)+
//                 (bufferBit[26] * 20)+
//                 (bufferBit[27] * 40)
//             );
// }

// uint8_t DcfTime::getHours(void)
// {
//     return  (   (bufferBit[29] *  1)+
//                 (bufferBit[30] *  2)+
//                 (bufferBit[31] *  4)+
//                 (bufferBit[32] *  8)+
//                 (bufferBit[33] * 10)+
//                 (bufferBit[34] * 20)
//             );
// }

// void DcfReceiver::Init(void)
// {
//     SignalInput::setInput(Gpio::InputType::PullUp);
//     SignalOutput::setOutput();
// }

// uint8_t edgeDetect(uint8_t sensorValue)
// {
//     static uint8_t sensorEdgePreValue = EDGE_UNKNOWN;
//     static uint8_t sensorEdgeValue    = EDGE_UNKNOWN;    
//     uint8_t edgeDetected = EDGE_UNKNOWN;

//     sensorEdgeValue = sensorValue;
//     if(sensorEdgePreValue != sensorEdgeValue)
//     {
//         sensorEdgePreValue  = sensorEdgeValue;
//         if(sensorValue == BIT_1)
//         {
//             edgeDetected = EDGE_RISING;
//         }
//         else if(sensorValue == BIT_0)
//         {
//             edgeDetected = EDGE_FALLING;            
//         }         
//     }

//     return edgeDetected;
// }

// uint8_t startBit(uint8_t edgeValue)
// {
//     static xpcc::Timestamp startBit_LETs( 0 );
//     static xpcc::Timestamp startBit_FETs( xpcc::Clock::now() );    
//     uint8_t startBitValue = BIT_UNKNOWN;

//     if(edgeValue == EDGE_RISING)
//     {
//         SignalOutput::reset();
//         startBit_LETs = xpcc::Clock::now();
//         startBit_FETs = xpcc::Clock::now();
//     }
//     else if(edgeValue == EDGE_FALLING)
//     {
//         SignalOutput::set();
//         startBit_FETs = xpcc::Clock::now();
//         if((startBit_FETs - startBit_LETs) >= (1800 - ((1800/100)*10)))
//         {
//             startBitValue = BIT_1;
//         }
//     }  

//     return startBitValue;
// }

// uint8_t regularBit(bool startBit, uint8_t edgeValue)
// {
//     static bool prevStartBit   = false;
//            bool detectStartBit = false;

//     static uint8_t regularBitValue = BIT_UNKNOWN;
//     static xpcc::ShortTimeout timeoutBitPeriod;
//     static xpcc::Timestamp timestampBitOne;
//     xpcc::Timestamp timestampBitOne_Now;

//     if(prevStartBit != startBit)
//     {
//         prevStartBit = startBit;
//         detectStartBit = startBit;
//     }

//     if(detectStartBit)
//     {
//         DcfTime::resetBufferBitIndex();
//     }

//     if(detectStartBit || timeoutBitPeriod.isExpired())
//     {
//         regularBitValue = BIT_UNKNOWN;
//         timeoutBitPeriod.restart(edgeValue == EDGE_RISING ? 1000 : 1000-10);
//         timestampBitOne = xpcc::Clock::now();
//     }

//     timestampBitOne_Now = xpcc::Clock::now() - timestampBitOne;

//     if((timestampBitOne_Now >= (200 - ((200/100)*10))) && (timestampBitOne_Now <= 200))
//     {
//         if(regularBitValue == BIT_UNKNOWN)
//         {
//             regularBitValue = (edgeValue == EDGE_RISING ? BIT_0 : BIT_1);
//             DcfTime::addBit(regularBitValue);
//         }
//     }

//     return regularBitValue;
// }

// void DcfReceiver::Process(void)
// {
//     uint8_t sensorValue;
//     uint8_t edgeValue;

//     sensorValue = ((!SignalInput::read()) ? BIT_1 : BIT_0);
//     edgeValue = edgeDetect(sensorValue);
//     StartBit = (startBit(edgeValue) == BIT_1 ? BIT_1 : BIT_0);
//     CurentBit = regularBit(StartBit, sensorValue);
// }

