
#include "nixieClock.hpp"

Nixie::GreenLed::GreenLed(void)
{
	GpioOutputB1::setOutput();
}

void Nixie::GreenLed::On(void)
{
	GpioOutputB1::reset();
}

void Nixie::GreenLed::Off(void)
{
	GpioOutputB1::set();
}

void Nixie::GreenLed::Toggle(void)
{
	GpioOutputB1::toggle();
}


//--Clock-------------------------------------------------------------------------------------
Nixie::Clock::Clock(void)
{
	this->greenLed.Off();
}

bool Nixie::Clock::Run(void)
{
	PT_BEGIN();

	// set everything up

	while (true)
	{
		//button.Process();
		// if(button.IsPressed())
		// {
		// 	this->greenLed.On();
		// 	this->display.Blink(true);
		// }
		// if(button.IsReleased())
		// {
		// 	this->greenLed.Off();
		// 	this->display.Blink(false);			
		// }
		// if(Button::IsPressedLong())
		// {
		// 	this->greenLed.On();
		// }
		// else
		// {
		// 	this->greenLed.Off();
		// }
		// if(Button::IsRepeated())
		// {
		// 	this->greenLed.On();
		// }
		// else
		// {
		// 	this->greenLed.Off();
		// }

		//display.Process();
		// this->greenLed.Toggle();
		this->timeout.restart(1);
		PT_WAIT_UNTIL(this->timeout.isExpired());
	}

	PT_END();
}

void Nixie::Clock::Test(void)
{
	static uint8_t lampIndex = 0x00;
	static uint8_t cathodeIndex = 0x00;

	//this->greenLed.Toggle();

	display.SetLamp(lampIndex, cathodeIndex);
    xpcc::delayMilliseconds(30);

    lampIndex++;
    if(lampIndex > 5)
    {
    	lampIndex = 0x00;
    	cathodeIndex++;
    	if(cathodeIndex > 9)
    	{
    		cathodeIndex = 0x00;
    	}
    }
}

void Nixie::Clock::SetTime_Enter(void)
{
	this->greenLed.On();
	this->display.Blink(true);
}

void Nixie::Clock::SetTime_Update(void)
{

}

void Nixie::Clock::SetTime_Exit(void)
{
	this->greenLed.Off();
	this->display.Blink(false);
}


Nixie::Clock nixieClock;

