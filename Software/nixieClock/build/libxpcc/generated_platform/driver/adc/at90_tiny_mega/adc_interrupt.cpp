// coding: utf-8
/* Copyright (c) 2011, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------

#include "adc_interrupt.hpp"
#include "../../../device.hpp"

// ----------------------------------------------------------------------------
xpcc::atmega::AdcInterrupt::Handler
xpcc::atmega::AdcInterrupt::handler(xpcc::dummy);

// ----------------------------------------------------------------------------
ISR(ADC_vect, ATTRIBUTE_WEAK)
{
	xpcc::atmega::AdcInterrupt::handler();
}
