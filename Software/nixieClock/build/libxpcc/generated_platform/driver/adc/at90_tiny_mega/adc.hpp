// coding: utf-8
/* Copyright (c) 2011, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------

/**
 * @ingroup 	atmega16
 * @defgroup	atmega16_adc ADC
 */

#ifndef XPCC_ATMEGA_ADC_HPP
#define XPCC_ATMEGA_ADC_HPP

#include <xpcc/architecture/interface/adc.hpp>
#include "../../../device.hpp"

#if defined (__AVR_ATmega64HVE__) || defined(__AVR_ATmega64HVE2__)
#	error "The ATmega64HVE2 is not supported by this ADC class."
#endif
namespace xpcc
{

namespace atmega
{

/**
 * Generic Analog/Digital-Converter module
 *
 * This class aims at providing a common interface to all the different
 * register layouts of the ADC modules in most ATmegas.
 * It takes into consideration restrictions and extensions in ADC
 * functionality and provides the appropriate methods to configure them.
 *
 * This class enables you to address and use a broader array of ATmegas
 * with similar ADC functionality without changing a single line of code.
 *
 * For best use of this class, check your device's datasheet for the
 * supported functionality.
 *
 * ADC clock frequency should be between 50 and 200 kHz for maximum
 * resolution.
 * If less than the full resolution of 10 bits are
 * needed the frequency can be higher.
 *
 * @author	Niklas Hauser
 * @ingroup	atmega16_adc
 */
class Adc : public xpcc::Adc
{
public:
	typedef uint8_t Channel;
	static constexpr uint8_t Resolution = 10;

	enum class
	Reference : uint8_t
	{
		ExternalRef = 0,
		InternalVcc = 0x40,
		Internal2V56 = 0xe0,
		Internal1V1 = 0xc0,
	};
private:
	enum class
	Prescaler : uint8_t
	{
		Div2 = 0x01,
		Div4 = 0x02,
		Div8 = 0x03,
		Div16 = 0x04,
		Div32 = 0x05,
		Div64 = 0x06,
		Div128 = 0x07,
	};

public:
	// start inherited documentation
	template< class clockSource, uint32_t frequency=100000,
			uint16_t tolerance = xpcc::Tolerance::TenPercent >
	static void
	initialize()
	{
		// calculate the nearest prescaler from the frequency
		constexpr float pre_raw = (static_cast<float>(clockSource::Adc) / frequency);
		constexpr uint16_t pre = (
				(pre_raw >= ( 64 * 4.f/3)) ? 128 : (
				(pre_raw >= ( 32 * 4.f/3)) ?  64 : (
				(pre_raw >= ( 16 * 4.f/3)) ?  32 : (
				(pre_raw >= (  8 * 4.f/3)) ?  16 : (
				(pre_raw >= (  4 * 4.f/3)) ?   8 : (
				(pre_raw >= (  2 * 4.f/3)) ?   4 :
											   2
				))))));

		// check if we found a prescaler which generates a frequency within the tolerance
		assertBaudrateInTolerance<
				clockSource::Adc / pre,
				frequency,
				tolerance >();

		// translate the prescaler into the bitmapping
		constexpr Prescaler prescaler = (
				(pre >= 128) ? Prescaler::Div128 : (
				(pre >=  64) ? Prescaler::Div64  : (
				(pre >=  32) ? Prescaler::Div32  : (
				(pre >=  16) ? Prescaler::Div16  : (
				(pre >=   8) ? Prescaler::Div8   : (
				(pre >=   4) ? Prescaler::Div4   :
							   Prescaler::Div2
				))))));

		disable();
		setPrescaler(prescaler);
		// enable the module
		ADCSRA |= (1<<ADEN);
	}

	static inline void
	disable()
	{
		ADCSRA &= ~(1<<ADEN);
	}


	static inline void
	startConversion()
	{
		acknowledgeInterruptFlag();
		ADCSRA |= (1<<ADSC);
	}

	static inline bool
	isConversionFinished()
	{
		return getInterruptFlag();
	}

	static inline uint16_t
	getValue()
	{
		return ADC;
	}


	/**
	 * Read the value an analog channel
	 *
	 * A normal conversion takes 13 ADC clock cycles. With a clock frequency
	 * of for example 200 kHz a conversion therefore needs 65 microseconds.
	 * This time increases with a lower frequency.
	 */
	static inline uint16_t
	readChannel(Channel channel)
	{
		if (!setChannel(channel)) return 0;

		startConversion();
		// wait until the conversion is finished
		while (!isConversionFinished())
			;

		return getValue();
	}


	static inline bool
	setChannel(Channel channel)
	{
		if (channel > 0x1f) return false;
		ADMUX = (ADMUX & ~0x1f) | (channel & 0x1f);
		return true;
	}

	static inline uint8_t
	getChannel()
	{
		return (ADMUX & 0x1f);
	}


	static inline void
	enableFreeRunningMode()
	{
		enableAutoTrigger();
		setAutoTriggerSource(0);
	}

	static inline void
	diableFreeRunningMode()
	{
		disableAutoTrigger();
	}


	static inline void
	setLeftAdjustResult()
	{
		ADMUX |= (1 << ADLAR);
	}

	static inline void
	setRightAdjustResult()
	{
		ADMUX &= ~(1 << ADLAR);
	}
	// stop inherited documentation



	/**
	 * voltage reference for the ADC
	 *
	 * The internal voltage reference options may not be used if an
	 * external reference voltage is being applied to the AREF pin.
	 */
	static inline void
	setReference(Reference reference)
	{
		ADMUX = (ADMUX & ~0xc0) | (uint8_t(reference) & 0xc0);
	}
	/// @return	`true` if the flag is set,
	///			`false` otherwise
	static inline bool
	getInterruptFlag()
	{
		return (ADCSRA & (1<<ADIF));
	}

	/// Clears the interrupt flag
	static inline void
	acknowledgeInterruptFlag()
	{
		ADCSRA &= ~(1<<ADIF);
	}

	/// Enables the ADC Conversion Complete Interrupt
	static inline void
	enableInterrupt()
	{
		ADCSRA |= (1<<ADIE);
	}

	/// Disables the ADC Conversion Complete Interrupt
	static inline void
	disableInterrupt()
	{
		ADCSRA &= ~(1<<ADIE);
	}

	/**
	 * Enable auto triggering of the ADC
	 *
	 * The ADC will start a conversion on a positive edge of the
	 * selected trigger signal.
	 * @see setAutoTriggerSource
	 */
	static inline void
	enableAutoTrigger()
	{
		ADCSRA |= (1<<ADATE);
	}

	/// Disable auto triggering of the ADC
	static inline void
	disableAutoTrigger()
	{
		ADCSRA &= ~(1<<ADATE);
	}

	/**
	 * Selects which source will trigger an ADC conversion
	 *
	 * A conversion will be triggered by the rising edge of the
	 * selected Interrupt Flag. Note that switching from a trigger
	 * source that is cleared to a trigger source that is set, will
	 * generate a positive edge on the trigger signal.
	 * Set to 0 to enable Free Running Mode.
	 */
	static inline void
	setAutoTriggerSource(uint8_t source)
	{
		if (source > 0x07) return;
		SFIOR = (SFIOR & ~0xe0) | (source << 5);
	}
	// MARK: special operation modes
private:
	static inline void
	setPrescaler(Prescaler prescaler)
	{
		ADCSRA = (ADCSRA & ~0x07) | static_cast<uint8_t>(prescaler);
	}
};

}	// namespace atmega

}	// namespace xpcc

#endif	// XPCC_ATMEGA_ADC_HPP