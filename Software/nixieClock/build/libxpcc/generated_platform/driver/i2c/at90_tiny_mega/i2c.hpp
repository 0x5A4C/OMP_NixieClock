// coding: utf-8
/* Copyright (c) 2013, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------
/*
 * WARNING: This file is generated automatically, do not edit!
 * Please modify the corresponding *.in file instead and rebuild this file.
 */
// ----------------------------------------------------------------------------

#ifndef XPCC_ATMEGA_I2C_HPP
#define XPCC_ATMEGA_I2C_HPP

/**
 * @ingroup 	atmega16
 * @defgroup	atmega16_i2c I2C
 */

namespace xpcc
{

namespace atmega
{

/// @ingroup	atmega16_i2c
struct I2c
{
	enum class
	Prescaler : uint8_t
	{
		Div1  = 0,
		Div4  = (1 << TWPS0),
		Div16 = (1 << TWPS1),
		Div64 = (1 << TWPS1) | (1 << TWPS0)
	};
};

} // namespace atmega

} // namespace xpcc

#endif	// XPCC_ATMEGA_I2C_HPP