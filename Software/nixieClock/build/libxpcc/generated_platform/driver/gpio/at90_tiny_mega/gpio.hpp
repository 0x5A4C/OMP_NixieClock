// coding: utf-8
/* Copyright (c) 2013, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------
/*
 * WARNING: This file is generated automatically, do not edit!
 * Please modify the corresponding *.in file instead and rebuild this file.
 */
// ----------------------------------------------------------------------------

#ifndef XPCC_ATMEGA_GPIO_HPP
#define XPCC_ATMEGA_GPIO_HPP

#include "../../../device.hpp"
#include "../../../type_ids.hpp"
#include "gpio_define.h"
#include <xpcc/architecture/interface/gpio.hpp>
#include <xpcc/architecture/interface/i2c.hpp>

/**
 * @ingroup 	platform
 * @defgroup	atmega16
 */

/**
 * @ingroup 	atmega16
 * @defgroup	atmega16_gpio GPIO
 */


namespace xpcc
{

namespace atmega
{

/// @ingroup	atmega16_gpio
struct Gpio
{
	/// Each Input Pin can be configured in one of these states.
	enum class
	InputType : uint8_t
	{
		Floating,	///< The input pin is left floating
		PullUp,		///< The input pin is pulled to Vcc
	};

	/// Each External Interrupt can be configured to trigger on these conditions.
	enum class
	InputTrigger : uint8_t
	{
		LowLevel = 0b00,	///< triggers **continuously** during low level
		BothEdges = 0b01,	///< triggers on both rising and falling edge
		FallingEdge = 0b10,	///< triggers on falling edge
		RisingEdge = 0b11,	///< triggers on rising edge
	};

	/// Available ports on this device.
	enum class
	Port
	{
		A,
		C,
		B,
		D,
	};

protected:
	/// @cond
	static constexpr uint8_t
	i(InputType config) { return static_cast<uint8_t>(config); }
	static constexpr uint8_t
	i(InputTrigger trigger) { return static_cast<uint8_t>(trigger); }
	/// @endcond
};

/**
 * Gpio OpenDrain template, which remaps the behavior of the Gpio pin to
 * simulate an open-drain output (with internal pullups if needed).
 * You must use this class for `SoftwareI2cMaster`!
 *
 * @see ::xpcc::SoftwareI2cMaster
 * @ingroup	atmega16_gpio
 * @{
 */
template< class Pin >
class GpioOpenDrain : public Pin
{
public:
	ALWAYS_INLINE static void configure(Gpio::InputType /*type*/) {}
	ALWAYS_INLINE static void setInput() {}
	ALWAYS_INLINE static void setInput(Gpio::InputType /*type*/) {}
	ALWAYS_INLINE static void setOutput() {}
	ALWAYS_INLINE static void setOutput(bool status) {
		set(status);
	}
	/// maps to `setInput(InputType::Floating)`
	ALWAYS_INLINE static void set() {
		Pin::setInput(Gpio::InputType::Floating);
	}
	/// maps to `setOutput(::xpcc::Gpio::Low)`
	ALWAYS_INLINE static void reset() {
		Pin::setOutput(::xpcc::Gpio::Low);
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareI2cMasterSda) {
		set();
	}
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareI2cMasterScl) {
		set();
		xpcc::I2c::resetDevices< Pin >();
		set();
	}
};
template< class Pin >
class GpioOpenDrainWithPullUp : public Pin
{
public:
	ALWAYS_INLINE static void configure(Gpio::InputType /*type*/) {}
	ALWAYS_INLINE static void setInput() {}
	ALWAYS_INLINE static void setInput(Gpio::InputType /*type*/) {}
	ALWAYS_INLINE static void setOutput() {}
	ALWAYS_INLINE static void setOutput(bool status) {
		set(status);
	}
	/// maps to `setInput(InputType::PullUp)`
	ALWAYS_INLINE static void set() {
		Pin::setInput(Gpio::InputType::PullUp);
	}
	/// maps to `setOutput(::xpcc::Gpio::Low)`
	ALWAYS_INLINE static void reset() {
		Pin::setOutput(::xpcc::Gpio::Low);
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareI2cMasterSda) {
		set();
	}
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareI2cMasterScl) {
		set();
		xpcc::I2c::resetDevices< Pin >();
		set();
	}
};
/// @}

/// Output class for Pin A0
/// @ingroup	atmega16_gpio
struct GpioOutputA0 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A0
/// @ingroup	atmega16_gpio
struct GpioInputA0 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A0
/// @ingroup	atmega16_gpio
struct GpioA0 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin A1
/// @ingroup	atmega16_gpio
struct GpioOutputA1 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A1
/// @ingroup	atmega16_gpio
struct GpioInputA1 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A1
/// @ingroup	atmega16_gpio
struct GpioA1 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin A2
/// @ingroup	atmega16_gpio
struct GpioOutputA2 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A2
/// @ingroup	atmega16_gpio
struct GpioInputA2 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A2
/// @ingroup	atmega16_gpio
struct GpioA2 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin A3
/// @ingroup	atmega16_gpio
struct GpioOutputA3 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A3
/// @ingroup	atmega16_gpio
struct GpioInputA3 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A3
/// @ingroup	atmega16_gpio
struct GpioA3 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin A4
/// @ingroup	atmega16_gpio
struct GpioOutputA4 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A4
/// @ingroup	atmega16_gpio
struct GpioInputA4 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A4
/// @ingroup	atmega16_gpio
struct GpioA4 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin A5
/// @ingroup	atmega16_gpio
struct GpioOutputA5 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A5
/// @ingroup	atmega16_gpio
struct GpioInputA5 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A5
/// @ingroup	atmega16_gpio
struct GpioA5 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin A6
/// @ingroup	atmega16_gpio
struct GpioOutputA6 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A6
/// @ingroup	atmega16_gpio
struct GpioInputA6 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A6
/// @ingroup	atmega16_gpio
struct GpioA6 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin A7
/// @ingroup	atmega16_gpio
struct GpioOutputA7 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin A7
/// @ingroup	atmega16_gpio
struct GpioInputA7 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin A7
/// @ingroup	atmega16_gpio
struct GpioA7 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::A;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRA |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTA |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTA &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTA |= mask;
		}
		else {
			PORTA &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINA & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B0
/// @ingroup	atmega16_gpio
struct GpioOutputB0 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B0
/// @ingroup	atmega16_gpio
struct GpioInputB0 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B0
/// @ingroup	atmega16_gpio
struct GpioB0 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B1
/// @ingroup	atmega16_gpio
struct GpioOutputB1 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B1
/// @ingroup	atmega16_gpio
struct GpioInputB1 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B1
/// @ingroup	atmega16_gpio
struct GpioB1 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B2
/// @ingroup	atmega16_gpio
struct GpioOutputB2 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B2
/// @ingroup	atmega16_gpio
struct GpioInputB2 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	ALWAYS_INLINE static void setInputTrigger(InputTrigger trigger) {
		MCUCSR = (MCUCSR & ~(1 << ISC2)) | ((i(trigger) & 0b01) << ISC2);
	}
	ALWAYS_INLINE static void enableExternalInterrupt() {
		EIMSK |= (1 << INT2);
	}
	ALWAYS_INLINE static void disableExternalInterrupt() {
		EIMSK &= ~(1 << INT2);
	}
	ALWAYS_INLINE static bool getExternalInterruptFlag() {
		return (EIFR & (1 << INTF2));
	}
	ALWAYS_INLINE static void acknowledgeExternalInterruptFlag() {
		EIFR |= (1 << INTF2);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B2
/// @ingroup	atmega16_gpio
struct GpioB2 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	ALWAYS_INLINE static void setInputTrigger(InputTrigger trigger) {
		MCUCSR = (MCUCSR & ~(1 << ISC2)) | ((i(trigger) & 0b01) << ISC2);
	}
	ALWAYS_INLINE static void enableExternalInterrupt() {
		EIMSK |= (1 << INT2);
	}
	ALWAYS_INLINE static void disableExternalInterrupt() {
		EIMSK &= ~(1 << INT2);
	}
	ALWAYS_INLINE static bool getExternalInterruptFlag() {
		return (EIFR & (1 << INTF2));
	}
	ALWAYS_INLINE static void acknowledgeExternalInterruptFlag() {
		EIFR |= (1 << INTF2);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B3
/// @ingroup	atmega16_gpio
struct GpioOutputB3 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B3
/// @ingroup	atmega16_gpio
struct GpioInputB3 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B3
/// @ingroup	atmega16_gpio
struct GpioB3 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B4
/// @ingroup	atmega16_gpio
struct GpioOutputB4 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect `B4` as `Ss` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterSs) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B4
/// @ingroup	atmega16_gpio
struct GpioInputB4 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B4` as `Ss` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSs) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B4
/// @ingroup	atmega16_gpio
struct GpioB4 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B4` as `Ss` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterSs) {
		setOutput();
	}
	/// Connect `B4` as `Ss` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSs) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B5
/// @ingroup	atmega16_gpio
struct GpioOutputB5 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect `B5` as `Somi` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSomi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B5
/// @ingroup	atmega16_gpio
struct GpioInputB5 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B5` as `Miso` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterMiso) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B5
/// @ingroup	atmega16_gpio
struct GpioB5 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B5` as `Miso` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterMiso) {
		setInput();
	}
	/// Connect `B5` as `Somi` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSomi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B6
/// @ingroup	atmega16_gpio
struct GpioOutputB6 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect `B6` as `Mosi` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B6
/// @ingroup	atmega16_gpio
struct GpioInputB6 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B6` as `Simo` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSimo) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B6
/// @ingroup	atmega16_gpio
struct GpioB6 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B6` as `Mosi` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterMosi) {
		setOutput();
	}
	/// Connect `B6` as `Simo` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSimo) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin B7
/// @ingroup	atmega16_gpio
struct GpioOutputB7 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	/// Connect `B7` as `Sck` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin B7
/// @ingroup	atmega16_gpio
struct GpioInputB7 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B7` as `Sck` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSck) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin B7
/// @ingroup	atmega16_gpio
struct GpioB7 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::B;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRB |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTB |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTB &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTB |= mask;
		}
		else {
			PORTB &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINB & mask);
	}
	/// Connect `B7` as `Sck` to `SpiMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiMasterSck) {
		setOutput();
	}
	/// Connect `B7` as `Sck` to `SpiSlave`.
	ALWAYS_INLINE static void
	connect(TypeId::SpiSlaveSck) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C0
/// @ingroup	atmega16_gpio
struct GpioOutputC0 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect `C0` as `Scl` to `I2cMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::I2cMasterScl) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C0
/// @ingroup	atmega16_gpio
struct GpioInputC0 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C0
/// @ingroup	atmega16_gpio
struct GpioC0 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect `C0` as `Scl` to `I2cMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::I2cMasterScl) {
		setOutput();
		xpcc::I2c::resetDevices< GpioC0 >();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C1
/// @ingroup	atmega16_gpio
struct GpioOutputC1 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect `C1` as `Sda` to `I2cMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::I2cMasterSda) {
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C1
/// @ingroup	atmega16_gpio
struct GpioInputC1 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect `C1` as `Sda` to `I2cMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::I2cMasterSda) {
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C1
/// @ingroup	atmega16_gpio
struct GpioC1 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect `C1` as `Sda` to `I2cMaster`.
	ALWAYS_INLINE static void
	connect(TypeId::I2cMasterSda) {
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C2
/// @ingroup	atmega16_gpio
struct GpioOutputC2 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C2
/// @ingroup	atmega16_gpio
struct GpioInputC2 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C2
/// @ingroup	atmega16_gpio
struct GpioC2 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C3
/// @ingroup	atmega16_gpio
struct GpioOutputC3 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C3
/// @ingroup	atmega16_gpio
struct GpioInputC3 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C3
/// @ingroup	atmega16_gpio
struct GpioC3 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C4
/// @ingroup	atmega16_gpio
struct GpioOutputC4 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C4
/// @ingroup	atmega16_gpio
struct GpioInputC4 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C4
/// @ingroup	atmega16_gpio
struct GpioC4 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C5
/// @ingroup	atmega16_gpio
struct GpioOutputC5 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C5
/// @ingroup	atmega16_gpio
struct GpioInputC5 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C5
/// @ingroup	atmega16_gpio
struct GpioC5 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C6
/// @ingroup	atmega16_gpio
struct GpioOutputC6 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C6
/// @ingroup	atmega16_gpio
struct GpioInputC6 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C6
/// @ingroup	atmega16_gpio
struct GpioC6 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin C7
/// @ingroup	atmega16_gpio
struct GpioOutputC7 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin C7
/// @ingroup	atmega16_gpio
struct GpioInputC7 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin C7
/// @ingroup	atmega16_gpio
struct GpioC7 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::C;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRC |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTC |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTC &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTC |= mask;
		}
		else {
			PORTC &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PINC & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D0
/// @ingroup	atmega16_gpio
struct GpioOutputD0 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D0
/// @ingroup	atmega16_gpio
struct GpioInputD0 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect `D0` as `Rxd` to `Uart0`.
	ALWAYS_INLINE static void
	connect(TypeId::Uart0Rxd) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D0
/// @ingroup	atmega16_gpio
struct GpioD0 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 0;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect `D0` as `Rxd` to `Uart0`.
	ALWAYS_INLINE static void
	connect(TypeId::Uart0Rxd) {
		setInput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D1
/// @ingroup	atmega16_gpio
struct GpioOutputD1 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect `D1` as `Txd` to `Uart0`.
	ALWAYS_INLINE static void
	connect(TypeId::Uart0Txd) {
		set();
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D1
/// @ingroup	atmega16_gpio
struct GpioInputD1 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D1
/// @ingroup	atmega16_gpio
struct GpioD1 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 1;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect `D1` as `Txd` to `Uart0`.
	ALWAYS_INLINE static void
	connect(TypeId::Uart0Txd) {
		set();
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D2
/// @ingroup	atmega16_gpio
struct GpioOutputD2 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D2
/// @ingroup	atmega16_gpio
struct GpioInputD2 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	ALWAYS_INLINE static void setInputTrigger(InputTrigger trigger) {
		MCUCR = (MCUCR & ~(0b11 << 2*0)) | (i(trigger) << 2*0);
	}
	ALWAYS_INLINE static void enableExternalInterrupt() {
		EIMSK |= (1 << INT0);
	}
	ALWAYS_INLINE static void disableExternalInterrupt() {
		EIMSK &= ~(1 << INT0);
	}
	ALWAYS_INLINE static bool getExternalInterruptFlag() {
		return (EIFR & (1 << INTF0));
	}
	ALWAYS_INLINE static void acknowledgeExternalInterruptFlag() {
		EIFR |= (1 << INTF0);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D2
/// @ingroup	atmega16_gpio
struct GpioD2 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 2;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	ALWAYS_INLINE static void setInputTrigger(InputTrigger trigger) {
		MCUCR = (MCUCR & ~(0b11 << 2*0)) | (i(trigger) << 2*0);
	}
	ALWAYS_INLINE static void enableExternalInterrupt() {
		EIMSK |= (1 << INT0);
	}
	ALWAYS_INLINE static void disableExternalInterrupt() {
		EIMSK &= ~(1 << INT0);
	}
	ALWAYS_INLINE static bool getExternalInterruptFlag() {
		return (EIFR & (1 << INTF0));
	}
	ALWAYS_INLINE static void acknowledgeExternalInterruptFlag() {
		EIFR |= (1 << INTF0);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D3
/// @ingroup	atmega16_gpio
struct GpioOutputD3 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D3
/// @ingroup	atmega16_gpio
struct GpioInputD3 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	ALWAYS_INLINE static void setInputTrigger(InputTrigger trigger) {
		MCUCR = (MCUCR & ~(0b11 << 2*1)) | (i(trigger) << 2*1);
	}
	ALWAYS_INLINE static void enableExternalInterrupt() {
		EIMSK |= (1 << INT1);
	}
	ALWAYS_INLINE static void disableExternalInterrupt() {
		EIMSK &= ~(1 << INT1);
	}
	ALWAYS_INLINE static bool getExternalInterruptFlag() {
		return (EIFR & (1 << INTF1));
	}
	ALWAYS_INLINE static void acknowledgeExternalInterruptFlag() {
		EIFR |= (1 << INTF1);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D3
/// @ingroup	atmega16_gpio
struct GpioD3 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 3;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	ALWAYS_INLINE static void setInputTrigger(InputTrigger trigger) {
		MCUCR = (MCUCR & ~(0b11 << 2*1)) | (i(trigger) << 2*1);
	}
	ALWAYS_INLINE static void enableExternalInterrupt() {
		EIMSK |= (1 << INT1);
	}
	ALWAYS_INLINE static void disableExternalInterrupt() {
		EIMSK &= ~(1 << INT1);
	}
	ALWAYS_INLINE static bool getExternalInterruptFlag() {
		return (EIFR & (1 << INTF1));
	}
	ALWAYS_INLINE static void acknowledgeExternalInterruptFlag() {
		EIFR |= (1 << INTF1);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D4
/// @ingroup	atmega16_gpio
struct GpioOutputD4 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D4
/// @ingroup	atmega16_gpio
struct GpioInputD4 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D4
/// @ingroup	atmega16_gpio
struct GpioD4 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 4;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D5
/// @ingroup	atmega16_gpio
struct GpioOutputD5 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D5
/// @ingroup	atmega16_gpio
struct GpioInputD5 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D5
/// @ingroup	atmega16_gpio
struct GpioD5 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 5;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D6
/// @ingroup	atmega16_gpio
struct GpioOutputD6 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D6
/// @ingroup	atmega16_gpio
struct GpioInputD6 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D6
/// @ingroup	atmega16_gpio
struct GpioD6 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 6;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// Output class for Pin D7
/// @ingroup	atmega16_gpio
struct GpioOutputD7 : public Gpio, ::xpcc::GpioOutput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
};

/// Input class for Pin D7
/// @ingroup	atmega16_gpio
struct GpioInputD7 : public Gpio, ::xpcc::GpioInput
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};

/// IO class for Pin D7
/// @ingroup	atmega16_gpio
struct GpioD7 : public Gpio, ::xpcc::GpioIO
{
public:
	static constexpr Port port = Port::D;		///< the port of this GPIO
	static constexpr uint8_t pin = 7;			///< the pin  of this GPIO
	static constexpr uint8_t mask = (1 << pin);	///< the mask of this GPIO

	ALWAYS_INLINE static void setOutput(bool status) {
		setOutput();
		set(status);
	}
	ALWAYS_INLINE static void setOutput() {
		DDRD |= mask;
	}
	ALWAYS_INLINE static void set() {
		PORTD |= mask;
	}
	ALWAYS_INLINE static void set(bool status) {
		if (status) { set(); }
		else { reset(); }
	}
	ALWAYS_INLINE static void reset() {
		PORTD &= ~mask;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= mask;
	}
	ALWAYS_INLINE static void configure(InputType type) {
		if (type == InputType::PullUp) {
			PORTD |= mask;
		}
		else {
			PORTD &= ~mask;
		}
	}
	ALWAYS_INLINE static void
	setInput(InputType type) {
		setInput();
		configure(type);
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~mask;
	}
	ALWAYS_INLINE static bool read() {
		return (PIND & mask);
	}
	/// Connect to `SoftwareSpiMasterMosi`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMosi) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterSck`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterSck) {
		setOutput();
	}
	/// Connect to `SoftwareSpiMasterMiso`.
	ALWAYS_INLINE static void
	connect(::xpcc::TypeId::SoftwareSpiMasterMiso) {
		setInput();
	}
};


/// @cond
template< Gpio::Port Port, uint8_t StartPin, uint8_t Width>
class GpioPortBase;
/// @endcond

/**
 * Creates a hardware port with StartPin as LSB and with Width.
 *
 * @tparam	StartPin	a Gpio pin
 * @tparam	Width		required width of the port (up to 8)
 * @ingroup atmega16_gpio
 */
template< class StartPin, uint8_t Width >
class GpioPort : public ::xpcc::GpioPort, /** @cond */ public GpioPortBase< StartPin::port, StartPin::pin, Width> /** @endcond */
{};

/// @cond
template< uint8_t StartPin, uint8_t Width >
class GpioPortBase<Gpio::Port::A, StartPin, Width> : public Gpio
{
	static_assert(StartPin < 8,
			"StartPin too large, maximum 7.");
	static_assert(Width <= 8,
			"Width too large, maximum 8.");
	static_assert(Width > 0,
			"Width should be at least 1.");
	static_assert(StartPin + Width <= 8,
			"StartPin + Width too large, maximum 8.");

	static constexpr uint8_t dataMask = (1 << Width) - 1;
	static constexpr uint8_t portMask = dataMask << StartPin;

public:
	static constexpr uint8_t width = Width;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRA |= portMask;
	}
	ALWAYS_INLINE static void setInput() {
		DDRA &= ~portMask;
	}
	ALWAYS_INLINE static uint8_t read() {
		uint8_t data = PINA & portMask;
		return (data >> StartPin);
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		data <<= StartPin;
		PORTA = (PORTA & ~portMask) | (data & portMask);
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= portMask;
	}
};
template<>
class GpioPortBase<Gpio::Port::A, 0, 8 > : public Gpio
{
public:
	static constexpr uint8_t width = 8;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRA = 0xff;
	}
	ALWAYS_INLINE static void setInput() {
		DDRA = 0;
	}
	ALWAYS_INLINE static uint8_t read() {
		return PINA;
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		PORTA = data;
	}
	ALWAYS_INLINE static void toggle() {
		PORTA ^= 0xff;
	}
};
template< uint8_t StartPin, uint8_t Width >
class GpioPortBase<Gpio::Port::C, StartPin, Width> : public Gpio
{
	static_assert(StartPin < 8,
			"StartPin too large, maximum 7.");
	static_assert(Width <= 8,
			"Width too large, maximum 8.");
	static_assert(Width > 0,
			"Width should be at least 1.");
	static_assert(StartPin + Width <= 8,
			"StartPin + Width too large, maximum 8.");

	static constexpr uint8_t dataMask = (1 << Width) - 1;
	static constexpr uint8_t portMask = dataMask << StartPin;

public:
	static constexpr uint8_t width = Width;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRC |= portMask;
	}
	ALWAYS_INLINE static void setInput() {
		DDRC &= ~portMask;
	}
	ALWAYS_INLINE static uint8_t read() {
		uint8_t data = PINC & portMask;
		return (data >> StartPin);
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		data <<= StartPin;
		PORTC = (PORTC & ~portMask) | (data & portMask);
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= portMask;
	}
};
template<>
class GpioPortBase<Gpio::Port::C, 0, 8 > : public Gpio
{
public:
	static constexpr uint8_t width = 8;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRC = 0xff;
	}
	ALWAYS_INLINE static void setInput() {
		DDRC = 0;
	}
	ALWAYS_INLINE static uint8_t read() {
		return PINC;
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		PORTC = data;
	}
	ALWAYS_INLINE static void toggle() {
		PORTC ^= 0xff;
	}
};
template< uint8_t StartPin, uint8_t Width >
class GpioPortBase<Gpio::Port::B, StartPin, Width> : public Gpio
{
	static_assert(StartPin < 8,
			"StartPin too large, maximum 7.");
	static_assert(Width <= 8,
			"Width too large, maximum 8.");
	static_assert(Width > 0,
			"Width should be at least 1.");
	static_assert(StartPin + Width <= 8,
			"StartPin + Width too large, maximum 8.");

	static constexpr uint8_t dataMask = (1 << Width) - 1;
	static constexpr uint8_t portMask = dataMask << StartPin;

public:
	static constexpr uint8_t width = Width;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRB |= portMask;
	}
	ALWAYS_INLINE static void setInput() {
		DDRB &= ~portMask;
	}
	ALWAYS_INLINE static uint8_t read() {
		uint8_t data = PINB & portMask;
		return (data >> StartPin);
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		data <<= StartPin;
		PORTB = (PORTB & ~portMask) | (data & portMask);
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= portMask;
	}
};
template<>
class GpioPortBase<Gpio::Port::B, 0, 8 > : public Gpio
{
public:
	static constexpr uint8_t width = 8;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRB = 0xff;
	}
	ALWAYS_INLINE static void setInput() {
		DDRB = 0;
	}
	ALWAYS_INLINE static uint8_t read() {
		return PINB;
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		PORTB = data;
	}
	ALWAYS_INLINE static void toggle() {
		PORTB ^= 0xff;
	}
};
template< uint8_t StartPin, uint8_t Width >
class GpioPortBase<Gpio::Port::D, StartPin, Width> : public Gpio
{
	static_assert(StartPin < 8,
			"StartPin too large, maximum 7.");
	static_assert(Width <= 8,
			"Width too large, maximum 8.");
	static_assert(Width > 0,
			"Width should be at least 1.");
	static_assert(StartPin + Width <= 8,
			"StartPin + Width too large, maximum 8.");

	static constexpr uint8_t dataMask = (1 << Width) - 1;
	static constexpr uint8_t portMask = dataMask << StartPin;

public:
	static constexpr uint8_t width = Width;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRD |= portMask;
	}
	ALWAYS_INLINE static void setInput() {
		DDRD &= ~portMask;
	}
	ALWAYS_INLINE static uint8_t read() {
		uint8_t data = PIND & portMask;
		return (data >> StartPin);
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		data <<= StartPin;
		PORTD = (PORTD & ~portMask) | (data & portMask);
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= portMask;
	}
};
template<>
class GpioPortBase<Gpio::Port::D, 0, 8 > : public Gpio
{
public:
	static constexpr uint8_t width = 8;

public:
	ALWAYS_INLINE static void setOutput() {
		DDRD = 0xff;
	}
	ALWAYS_INLINE static void setInput() {
		DDRD = 0;
	}
	ALWAYS_INLINE static uint8_t read() {
		return PIND;
	}
	ALWAYS_INLINE static void write(uint8_t data) {
		PORTD = data;
	}
	ALWAYS_INLINE static void toggle() {
		PORTD ^= 0xff;
	}
};
/// @endcond

}	// namespace atmega

}	// namespace xpcc

#endif