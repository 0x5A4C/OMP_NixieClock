// coding: utf-8
/* Copyright (c) 2013, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------
/*
 * WARNING: This file is generated automatically, do not edit!
 * Please modify the corresponding *.in file instead and rebuild this file.
 */
// ----------------------------------------------------------------------------

#ifndef XPCC_AVR_INTERRUPTS_HPP
#define XPCC_AVR_INTERRUPTS_HPP

#include "../../../device.hpp"

/**
 * @ingroup 	atmega16
 * @defgroup	atmega16_core Core
 */

namespace xpcc
{

/// @ingroup	atmega16_core
namespace atmega
{

/// enables global interrupts
static ALWAYS_INLINE void
enableInterrupts()
{
	sei();
}

/// disables global interrupts
static ALWAYS_INLINE void
disableInterrupts()
{
	cli();
}

}	// namespace atmega

}	// namespace xpcc


#endif	// XPCC_AVR_INTERRUPTS_HPP