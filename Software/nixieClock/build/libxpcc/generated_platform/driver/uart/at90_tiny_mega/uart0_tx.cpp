// coding: utf-8
/* Copyright (c) 2009, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------
/*
 * WARNING: This file is generated automatically, do not edit!
 * Please modify the corresponding *.in file instead and rebuild this file.
 */
// ----------------------------------------------------------------------------

#include <xpcc/architecture/driver/atomic/queue.hpp>
#include <xpcc/architecture/driver/atomic/lock.hpp>

#include "uart_defines.h"
#include "uart0.hpp"

static xpcc::atomic::Queue<uint8_t, 64> txBuffer;

// ----------------------------------------------------------------------------
ISR(USART0_UDRE_vect)
{
	if (txBuffer.isEmpty())
	{
		// transmission finished, disable UDRE interrupt
		UCSR0B &= ~(1 << UDRIE0);
	}
	else {
		// get one byte from buffer and write it to the UART buffer
		// which starts the transmission
		UDR0 = txBuffer.get();
		txBuffer.pop();
	}
}

// MARK: - write blocking
void
xpcc::atmega::Uart0::writeBlocking(uint8_t data)
{
	// wait until there is some place in the buffer
	while (!write(data))
		;

	// wait until everything has been sent
	while (!isWriteFinished())
		;
}

void
xpcc::atmega::Uart0::writeBlocking(const uint8_t *data, std::size_t length)
{
	// first push everything into the buffer
	for (std::size_t i = 0; i < length; ++i)
	{
		while (!write(*data++))
			;
	}

	// then wait
	while (!isWriteFinished())
		;
}

void
xpcc::atmega::Uart0::flushWriteBuffer()
{
	// just wait until the last byte has been sent
	while (!isWriteFinished())
		;
}

// MARK: - write
bool
xpcc::atmega::Uart0::write(uint8_t data)
{
	if (!txBuffer.push(data))
		return false;

	::xpcc::atomic::Lock lock;

	// enable UDRE interrupt
	UCSR0B |= (1 << UDRIE0);

	return true;
}

std::size_t
xpcc::atmega::Uart0::write(const uint8_t *data, std::size_t length)
{
	for (std::size_t i = 0; i < length; ++i)
	{
		if (!write(*data++))
		{
			return i;
		}
	}

	return length;
}

bool
xpcc::atmega::Uart0::isWriteFinished()
{
	return (txBuffer.isEmpty() && !(UCSR0B & (1 << UDRIE0)));
}

// MARK: - discard
std::size_t
xpcc::atmega::Uart0::discardTransmitBuffer()
{
	{
		::xpcc::atomic::Lock lock;
		UCSR0B &= ~(1 << UDRIE0);
	}

	std::size_t i = 0;
	while(!txBuffer.isEmpty())
	{
		txBuffer.pop();
		++i;
	}

	return i;
}
