// coding: utf-8
/* Copyright (c) 2009, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------
/*
 * WARNING: This file is generated automatically, do not edit!
 * Please modify the corresponding *.in file instead and rebuild this file.
 */
// ----------------------------------------------------------------------------

#include <xpcc/architecture/driver/atomic/queue.hpp>
#include <xpcc/architecture/driver/atomic/lock.hpp>

#include "uart_defines.h"
#include "uart0.hpp"

static xpcc::atomic::Queue<uint8_t, 8> rxBuffer;
static uint8_t error;

// ----------------------------------------------------------------------------
ISR(USART0_RX_vect)
{
	// first save the errors
	error |= UCSR0A & ((1 << FE0) | (1 << DOR0));
	// then read the buffer
	uint8_t data = UDR0;
	// push it into the buffer
	rxBuffer.push(data);
}

// ----------------------------------------------------------------------------
void
xpcc::atmega::Uart0::initialize(uint16_t ubrr)
{
	atomic::Lock lock;
	// Set baud rate
	if (ubrr & 0x8000) {
		UCSR0A = (1 << U2X0);  //Enable 2x speed 
		ubrr &= ~0x8000;
	}
	else {
		UCSR0A = 0;
	}
	UBRR0L = (uint8_t)  ubrr;
	UBRR0H = (uint8_t) (ubrr >> 8);
	
	// Enable USART receiver and transmitter and receive complete interrupt
	UCSR0B = (1 << RXCIE0) | (1 << RXEN0) | (1 << TXEN0);
	
	// Set frame format: asynchronous, 8data, no parity, 1stop bit
#ifdef URSEL0
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00) | (1 << URSEL0);
#else
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);
#endif
}

// MARK: - read
bool
xpcc::atmega::Uart0::read(uint8_t& data)
{
	if (rxBuffer.isEmpty())
		return false;
	
	data = rxBuffer.get();
	rxBuffer.pop();
	
	return true;
}

std::size_t
xpcc::atmega::Uart0::read(uint8_t *buffer, std::size_t length)
{
	for (std::size_t i = 0; i < length; ++i)
	{
		if (rxBuffer.isEmpty()) {
			return length;
		}
		else {
			*buffer++ = rxBuffer.get();
			rxBuffer.pop();
		}
	}
	
	return length;
}

// MARK: - discard
std::size_t
xpcc::atmega::Uart0::discardReceiveBuffer()
{
	std::size_t i(0);
	while(!rxBuffer.isEmpty())
	{
		rxBuffer.pop();
		++i;
	}
	
#if defined (RXC0)
	uint8_t c;
	while (UCSR0A & (1 << RXC0))
	{
		c = UDR0;
		(void) c;
	}
#endif
	
	return i;
}

// MARK: - error
uint8_t
xpcc::atmega::Uart0::getErrorFlags()
{
	return error;
}

void
xpcc::atmega::Uart0::acknowledgeErrorFlags()
{
	error = 0;
}
