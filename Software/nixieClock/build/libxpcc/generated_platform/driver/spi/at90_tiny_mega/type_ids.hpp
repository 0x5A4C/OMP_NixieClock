// coding: utf-8
/* Copyright (c) 2013, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------
/*
 * WARNING: This file is generated automatically, do not edit!
 * Please modify the corresponding *.in file instead and rebuild this file.
 */
// ----------------------------------------------------------------------------

#ifndef XPCC_ATMEGA_SPI_TYPE_IDS_HPP
#define XPCC_ATMEGA_SPI_TYPE_IDS_HPP

namespace xpcc
{

namespace atmega
{

namespace TypeId
{
	typedef struct{} SpiMasterMosi;
	typedef struct{} SpiMasterMiso;
	typedef struct{} SpiMasterSck;
	typedef struct{} SpiMasterSs;

	typedef struct{} SpiSlaveSimo;
	typedef struct{} SpiSlaveSomi;
	typedef struct{} SpiSlaveSck;
	typedef struct{} SpiSlaveSs;
}

} // namespace atmega

} // namespace xpcc

#endif // XPCC_ATMEGA_SPI_TYPE_IDS_HPP