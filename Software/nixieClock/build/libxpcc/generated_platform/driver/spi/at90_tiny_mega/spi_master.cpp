// coding: utf-8
/* Copyright (c) 2013, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------

#include "spi_master.hpp"
#include <xpcc/architecture/interface/register.hpp>
#include <xpcc/architecture/driver/atomic.hpp>

// bit 7 (0x80) is used for transfer 1 byte
// bit 6 (0x40) is used for transfer multiple byte
// bit 5-0 (0x3f) are used to store the aquire count
uint8_t
xpcc::atmega::SpiMaster::state(0);

void *
xpcc::atmega::SpiMaster::context(nullptr);
// ----------------------------------------------------------------------------

void
xpcc::atmega::SpiMaster::initialize(Prescaler prescaler)
{
	xpcc::atomic::Lock lock;

	SPCR = (1 << SPE) | (1 << MSTR) | (static_cast<uint8_t>(prescaler) & ~0x80);
	SPSR = (static_cast<uint8_t>(prescaler) & 0x80) ? (1 << SPI2X) : 0;
	state &= 0x3f;
}
// ----------------------------------------------------------------------------

uint8_t
xpcc::atmega::SpiMaster::aquire(void *ctx)
{
	if (context == nullptr)
	{
		context = ctx;
		state = (state & ~0x3f) | 1;
		return 1;
	}

	if (ctx == context)
		return (++state & 0x3f);

	return 0;
}

uint8_t
xpcc::atmega::SpiMaster::release(void *ctx)
{
	if (ctx == context)
	{
		if ((--state & 0x3f) == 0)
			context = nullptr;
	}
	return (state & 0x3f);
}
// ----------------------------------------------------------------------------

xpcc::co::Result<uint8_t>
xpcc::atmega::SpiMaster::transfer(uint8_t data)
{
	// this is a manually implemented "fast coroutine"
	// there is no context or nesting protection, since we don't need it.
	// there are only two states encoded into 1 bit (LSB of state):
	//   1. waiting to start, and
	//   2. waiting to finish.

	// MSB != Bit7 ?
	if ( !(state & Bit7) )
	{
		// start transfer by copying data into register
		SPDR = data;

		// set MSB = Bit7
		state |= Bit7;
	}

	// wait for transfer to finish
	if (!(SPSR & (1 << SPIF)))
		return {xpcc::co::Running};

	data = SPDR;
	state &= ~Bit7;
	return {xpcc::co::Stop, data};
}

xpcc::co::Result<void>
xpcc::atmega::SpiMaster::transfer(
		uint8_t *tx, uint8_t *rx, std::size_t length)
{
	// this is a manually implemented "fast coroutine"
	// there is no context or nesting protection, since we don't need it.
	// there are only two states encoded into 1 bit (Bit6 of state):
	//   1. initialize index, and
	//   2. wait for 1-byte transfer to finish.

	// we need to globally remember which byte we are currently transferring
	static std::size_t index = 0;

	// we are only interested in Bit6
	switch(state & Bit6)
	{
		case 0:
			// we will only visit this state once
			state |= Bit6;

			// initialize index and check range
			index = 0;
			while (index < length)
			{
		default:
		{
				// call the coroutine
				xpcc::co::Result<uint8_t> result = transfer(tx ? tx[index] : 0);

				// if the coroutine is still running, so are we
				if (result.state > xpcc::co::NestingError)
					return {xpcc::co::Running};

				// if rx != 0, we copy the result into the array
				if (rx) rx[index] = result.result;
		}
				index++;
			}

			// clear the state
			state &= ~Bit6;
			return {xpcc::co::Stop};
	}
}