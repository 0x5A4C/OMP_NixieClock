// coding: utf-8
/* Copyright (c) 2013, Roboterclub Aachen e.V.
 * All Rights Reserved.
 *
 * The file is part of the xpcc library and is released under the 3-clause BSD
 * license. See the file `LICENSE` for the full license governing this code.
 */
// ----------------------------------------------------------------------------
/*
 * WARNING: This file is generated automatically, do not edit!
 * Please modify the corresponding *.in file instead and rebuild this file.
 */
// ----------------------------------------------------------------------------

#ifndef XPCC_ATMEGA_SPI_HPP
#define XPCC_ATMEGA_SPI_HPP

/**
 * @ingroup 	atmega16
 * @defgroup	atmega16_spi SPI
 */

namespace xpcc
{

namespace atmega
{

/// @ingroup	atmega16_spi
struct Spi
{
	enum class
	Prescaler : uint8_t
	{
		Div2 = 0x80 | 0,
		Div4 = 0,
		Div8 = 0x80 | (1 << SPR0),
		Div16 = (1 << SPR0),
		Div32 = 0x80 | (1 << SPR1),
		Div64 = (1 << SPR1),
		Div128 = (1 << SPR1) | (1 << SPR0),
	};
};

} // namespace atmega

} // namespace xpcc

#endif	// XPCC_ATMEGA_SPI_HPP