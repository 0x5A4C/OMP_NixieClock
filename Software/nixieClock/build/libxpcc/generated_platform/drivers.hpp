// coding: utf-8
// ----------------------------------------------------------------------------
/* Copyright (c) 2013, Roboterclub Aachen e.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Roboterclub Aachen e.V. nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY ROBOTERCLUB AACHEN E.V. ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL ROBOTERCLUB AACHEN E.V. BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// ----------------------------------------------------------------------------

#ifndef XPCC__DRIVERS_HPP
#define XPCC__DRIVERS_HPP

// Include driver header files
// gpio/generic
#include "driver/gpio/generic/gpio.hpp"
// spi/generic
#include "driver/spi/generic/type_ids.hpp"
#include "driver/spi/generic/spi_master.hpp"
// one_wire/generic
#include "driver/one_wire/generic/one_wire_master.hpp"
// i2c/generic
#include "driver/i2c/generic/type_ids.hpp"
#include "driver/i2c/generic/i2c_master.hpp"
// clock/generic
#include "driver/clock/generic/common_clock.hpp"
// core/avr
#include "driver/core/avr/ram.hpp"
#include "driver/core/avr/interrupts.hpp"
// adc/at90_tiny_mega
#include "driver/adc/at90_tiny_mega/adc.hpp"
#include "driver/adc/at90_tiny_mega/adc_interrupt.hpp"
// clock/avr
#include "driver/clock/avr/static.hpp"
// i2c/at90_tiny_mega
#include "driver/i2c/at90_tiny_mega/i2c_master.hpp"
#include "driver/i2c/at90_tiny_mega/type_ids.hpp"
#include "driver/i2c/at90_tiny_mega/i2c.hpp"
// spi/at90_tiny_mega
#include "driver/spi/at90_tiny_mega/spi_master.hpp"
#include "driver/spi/at90_tiny_mega/type_ids.hpp"
#include "driver/spi/at90_tiny_mega/spi.hpp"
// timer/atmega
// uart/at90_tiny_mega
#include "driver/uart/at90_tiny_mega/uart0.hpp"
#include "driver/uart/at90_tiny_mega/type_ids.hpp"
#include "driver/uart/at90_tiny_mega/uart_defines.h"
// gpio/at90_tiny_mega
#include "driver/gpio/at90_tiny_mega/gpio.hpp"
#include "driver/gpio/at90_tiny_mega/gpio_define.h"
#endif	// XPCC__DRIVERS_HPP
