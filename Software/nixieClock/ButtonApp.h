// #include "AppConfig.h"

// #ifdef __AVR__
// #include <avr/io.h>
// #endif

// #include "Button.h"

#define DEBOUNCE_TIME         25
#define SAMPLE_FREQUENCY      1
#define MAXIMUM               (DEBOUNCE_TIME * SAMPLE_FREQUENCY)
#define MAXIMUM_LONG_P        (MAXIMUM * 40)
#define DOUBLECLICKTIMEOUT	  (MAXIMUM * 5)

