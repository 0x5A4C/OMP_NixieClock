#ifndef NIXIE_DCF_HPP
#define NIXIE_DCF_HPP

#include <xpcc/ui/time/time.hpp>

#include <xpcc/architecture/platform.hpp>
#include <xpcc/architecture/interface/gpio.hpp>

using namespace xpcc::atmega;
typedef GpioB0  SignalInput;
typedef	GpioOutputB1 SignalOutput;

typedef GpioA4 SCLPin;
typedef GpioA2 SDAPin;

            // SCLPin::set();
            // SDAPin::set();
            // SCLPin::reset();
            // SDAPin::reset();
/*----------------------------------------------------------------------------------------------------------------------------*/
typedef enum SignalValue
{
	SignalValue_High     = 0x01,
	SignalValue_Low      = 0x00,
	SignalValue_Unknown  = 0xFF,	
}SignalValue_t;

typedef enum SignalEdge
{
	SignalEdge_Rising     = 0x01,
	SignalEdge_Falling    = 0x02,
	SignalEdge_Unknown    = 0xFF,
}SignalEdge_t;

template<class T>
class DcfSignalReceiver: public xpcc::Date
{
private:
	uint8_t bufferBit[60] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	uint8_t bufferBitIndex = 0x00;

private:
public:
	SignalEdge_t  signalEdge 		= SignalEdge_Unknown;
	SignalValue_t startBit   		= SignalValue_Unknown;	
    SignalValue_t regularBitValue	= SignalValue_Unknown;

    //receiver
    DcfSignalReceiver();
    void EdgeDetect(void);
	void StartBit(void);
	void RegularBit(void);

	//clock
	void Decode(void);

	void ResetBufferBitIndex(void);
	void AddBit(SignalValue_t bit);
	void Process(void);
};

template <class T>
DcfSignalReceiver<T>::DcfSignalReceiver()
{
	T::setInput(Gpio::InputType::PullUp);

	SCLPin::setOutput();
	SDAPin::setOutput();
}

template <class T>
void DcfSignalReceiver<T>::EdgeDetect(void)
{ 
    static uint8_t sensorEdgePreValue = SignalEdge_Unknown;
    //FIXME: This is for inverted signal
    SignalValue_t signalValue = ((!T::read()) ? SignalValue_High : SignalValue_Low);

    this->signalEdge = SignalEdge_Unknown;

    if(sensorEdgePreValue != signalValue)
    {
        sensorEdgePreValue  = signalValue;
        if(signalValue == SignalValue_High)
        {
            this->signalEdge = SignalEdge_Rising;
        }
        else if(signalValue == SignalValue_Low)
        {
            this->signalEdge = SignalEdge_Falling;
        }         
    }
}

template <class T>
void DcfSignalReceiver<T>::StartBit(void)
{
    static xpcc::Timestamp startBit_LETs(0);
    static xpcc::Timestamp startBit_FETs(xpcc::Clock::now());
    
    this->startBit = SignalValue_Unknown;
	SDAPin::reset();
    if(this->signalEdge == SignalEdge_Rising)
    {
        SignalOutput::reset();
        startBit_LETs = xpcc::Clock::now();
        startBit_FETs = xpcc::Clock::now();
    }
    else if(this->signalEdge == SignalEdge_Falling)
    {
        SignalOutput::set();
        startBit_FETs = xpcc::Clock::now();
        if((startBit_FETs - startBit_LETs) >= (1800 - ((1800/100)*10)))
        {
            this->startBit = SignalValue_High;
            SDAPin::set();
        }
    }  
}

template <class T>
void DcfSignalReceiver<T>::RegularBit(void)
{
    static SignalValue_t prevStartBit    = SignalValue_Low;
		   SignalValue_t detectStartBit  = SignalValue_Low;

    static xpcc::ShortTimeout timeoutBitPeriod;
    static xpcc::Timestamp timestampBitOne;
    xpcc::Timestamp timestampBitOne_Now;

//	this->regularBitValue = SignalValue_Unknown;

    if(prevStartBit != this->startBit)
    {
        prevStartBit   = this->startBit;
        detectStartBit = this->startBit;
    }

    if(detectStartBit)
    {
        this->ResetBufferBitIndex();
    }

    if((detectStartBit == SignalValue_High ) || timeoutBitPeriod.isExpired())
    {
//    	SDAPin::reset();
        this->regularBitValue = SignalValue_Unknown;
        timeoutBitPeriod.restart(this->signalEdge == SignalEdge_Rising ? 1000 : 1000-10);
        timestampBitOne = xpcc::Clock::now();
    }

    timestampBitOne_Now = xpcc::Clock::now() - timestampBitOne;

    if((timestampBitOne_Now >= (200 - ((200/100)*10))) && (timestampBitOne_Now <= 200))
    {
        if(this->regularBitValue == SignalValue_Unknown)
        {
        	//FIXME
            this->regularBitValue = ((T::read()) ? SignalValue_High : SignalValue_Low);
            //this->regularBitValue = ((this->signalEdge == SignalEdge_Rising) ? SignalValue_High : SignalValue_Low);
            this->AddBit(this->regularBitValue);
//            SDAPin::set();
			if(this->regularBitValue == SignalValue_High)
				SCLPin::set();
			else
				SCLPin::reset();
        }
    }
}

template <class T>
void DcfSignalReceiver<T>::Decode(void)
{
	this->second = this->bufferBitIndex;

	this->minute = (    (this->bufferBit[21] *  1)+
		                (this->bufferBit[22] *  2)+
		                (this->bufferBit[23] *  4)+
		                (this->bufferBit[24] *  8)+
		                (this->bufferBit[25] * 10)+
		                (this->bufferBit[26] * 20)+
		                (this->bufferBit[27] * 40)
		            );

    this->hour = 	( 	(this->bufferBit[29] *  1)+
		                (this->bufferBit[30] *  2)+
		                (this->bufferBit[31] *  4)+
		                (this->bufferBit[32] *  8)+
		                (this->bufferBit[33] * 10)+
		                (this->bufferBit[34] * 20)
            		);

}

template <class T>
void DcfSignalReceiver<T>::ResetBufferBitIndex(void)
{
	this->bufferBitIndex = 0x00;
}

template <class T>
void DcfSignalReceiver<T>::AddBit(SignalValue_t bit)
{
    if(this->bufferBitIndex < 58)
    {
        this->bufferBit[this->bufferBitIndex ] = bit;
        this->bufferBitIndex += 1;
    }
}

template <class T>
void DcfSignalReceiver<T>::Process(void)
{
	this->EdgeDetect();
    this->StartBit();
    this->RegularBit();

	// if(this->startBit == SignalValue_High)
	// {
	// 	this->ResetBufferBitIndex();
	// }

 //    if(this->regularBitValue != SignalValue_Unknown)
 //    {
 //    	this->AddBit(this->regularBitValue);
 //    }

    this->Decode();
}

#endif
