#ifndef PCF8583_HPP
#define PCF8583_HPP

#include <xpcc/architecture/platform.hpp>
#include <xpcc/architecture/platform/driver/i2c/generic/i2c_master.hpp>
#include <xpcc/architecture/interface/i2c_device.hpp>
#include <xpcc/architecture/interface/i2c_transaction.hpp>

//Slave addresses: A1h or A3h for reading, A0h or A2h for writing
#define I2CADDR (0xA0>>1)

using namespace xpcc::atmega;
typedef GpioA4 SCLPin;
typedef GpioA2 SDAPin;
xpcc::atmega::GpioOpenDrain< GpioA4 > SCLPinOd;
xpcc::atmega::GpioOpenDrain< GpioA2 > SDAPinOd;

namespace xpcc
{
	template <typename I2cMaster>
	class PCF8583 : public xpcc::I2cDevice<I2cMaster>, protected xpcc::co::NestedCoroutine<>
	{
	public:
	PCF8583(uint8_t address = I2CADDR);

	xpcc::co::Result<bool>
	ping();

	/**
	 * Write byte
	 *
	 * @param	address		Address
	 * @param	data		Data byte
	 *
	 * @return	`true`	if the data could be written,
	 * 			`false` otherwise
	 */
	inline xpcc::co::Result<bool>
	writeByte(uint16_t address, uint8_t data)
	{
		return write(address, &data, 1);
	}

	/**
	 * Write block
	 *
	 * @param	address		Address
	 * @param	data		Data block
	 * @param	length		Number of bytes to be written
	 *
	 * @return	`true`	if the data could be written,
	 * 			`false` otherwise
	 */
	xpcc::co::Result<bool>
	write(uint16_t address, const uint8_t *data, std::size_t length);

	/**
	 * Convenience function
	 *
	 * Shortcut for:
	 * @code
	 * return write(address, static_cast<const uint8_t *>(&data), sizeof(T));
	 * @endcode
	 */
	template <typename T>
	inline xpcc::co::Result<bool>
	write(uint16_t address, const T& data)
	{
		return write(address, static_cast<const uint8_t *>(&data), sizeof(T));
	}

	/// Read byte
	inline xpcc::co::Result<bool>
	readByte(uint16_t address, uint8_t &data)
	{
		return read(address, &data, 1);
	}

	/// Read block
	xpcc::co::Result<bool>
	read(uint16_t address, uint8_t *data, std::size_t length);

	/**
	 * Convenience function
	 *
	 * Shortcut for:
	 * @code
	 * return read(address, static_cast<uint8_t *>(&data), sizeof(T));
	 * @endcode
	 */
	template <typename T>
	inline xpcc::co::Result<bool>
	read(uint16_t address, T& data)
	{
		return read(address, static_cast<uint8_t *>(&data), sizeof(T));
	}

	private:
		enum I2cTask : uint8_t
		{
			Idle = 0,
			Ping,
			Write,
			Read,
		};

		class DataTransmissionAdapter : public xpcc::I2cWriteReadAdapter
		{
		public:
			DataTransmissionAdapter(uint8_t address);

			bool inline
			configureWrite(uint16_t address, const uint8_t *buffer, std::size_t size);

			bool inline
			configureRead(uint16_t address, uint8_t *buffer, std::size_t size);

		protected:
			virtual Writing
			writing() override;

			uint8_t addressBuffer[2];
			bool writeAddress;
		};

		volatile uint8_t i2cTask;
		volatile uint8_t i2cSuccess;
		xpcc::I2cTagAdapter<DataTransmissionAdapter> adapter;
	};	
}

// ----------------------------------------------------------------------------
// MARK: PCF8583
template <typename I2cMaster>
xpcc::PCF8583<I2cMaster>::PCF8583(uint8_t address)
:	i2cTask(I2cTask::Idle), i2cSuccess(0), adapter(address, i2cTask, i2cSuccess)
{
}

// // MARK: - write operations
template <typename I2cMaster>
xpcc::co::Result<bool>
xpcc::PCF8583<I2cMaster>::ping()
{
	CO_BEGIN();

	CO_WAIT_UNTIL(adapter.configurePing() and
			(i2cTask = I2cTask::Ping, this->startTransaction(&adapter)));

	CO_WAIT_WHILE(i2cTask == I2cTask::Ping);

	CO_END_RETURN(i2cSuccess == I2cTask::Ping);
}

template <typename I2cMaster>
xpcc::co::Result<bool>
xpcc::PCF8583<I2cMaster>::write(uint16_t address, const uint8_t *data, std::size_t length)
{
	CO_BEGIN();

	CO_WAIT_UNTIL(adapter.configureWrite(address, data, length) and
			(i2cTask = I2cTask::Write, this->startTransaction(&adapter)));

	CO_WAIT_WHILE(i2cTask == I2cTask::Write);

	CO_END_RETURN(i2cSuccess == I2cTask::Write);
}

// MARK: - read operations
template <typename I2cMaster>
xpcc::co::Result<bool>
xpcc::PCF8583<I2cMaster>::read(uint16_t address, uint8_t *data, std::size_t length)
{
	CO_BEGIN();

	CO_WAIT_UNTIL(adapter.configureRead(address, data, length) and (i2cTask = I2cTask::Read, this->startTransaction(&adapter)));

	CO_WAIT_WHILE(i2cTask == I2cTask::Read);

	CO_END_RETURN(i2cSuccess == I2cTask::Read);
}
// ----------------------------------------------------------------------------
// MARK: DataTransmissionAdapter
template < class I2cMaster >
xpcc::PCF8583<I2cMaster>::DataTransmissionAdapter::DataTransmissionAdapter(uint8_t address)
:	I2cWriteReadAdapter(address), writeAddress(false)
{}

template < class I2cMaster >
bool
xpcc::PCF8583<I2cMaster>::DataTransmissionAdapter::configureWrite(uint16_t address, const uint8_t *buffer, std::size_t size)
{
	if (I2cWriteReadAdapter::configureWrite(buffer, size))
	{
		addressBuffer[0] = address >> 8;
		addressBuffer[1] = address;
		writeAddress = true;
		return true;
	}
	return false;
}

template < class I2cMaster >
bool
xpcc::PCF8583<I2cMaster>::DataTransmissionAdapter::configureRead(uint16_t address, uint8_t *buffer, std::size_t size)
{
	addressBuffer[0] = address >> 8;
	addressBuffer[1] = address;
	writeAddress = false;
	return I2cWriteReadAdapter::configureWriteRead(addressBuffer, 2, buffer, size);
}

template < class I2cMaster >
xpcc::I2cTransaction::Writing
xpcc::PCF8583<I2cMaster>::DataTransmissionAdapter::writing()
{
	if (writeAddress)
	{
		writeAddress = false;
		return Writing(addressBuffer, 2, OperationAfterWrite::Write);
	}
	return I2cWriteReadAdapter::writing();
}

#endif