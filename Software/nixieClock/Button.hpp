#ifndef BUTTON_HPP
#define BUTTON_HPP

#include <xpcc/architecture/interface/gpio.hpp>
#include <xpcc/processing/timer.hpp>
#include "nixieDisplayApl.hpp"

namespace Nixie
{
		typedef enum
		{
			eButtonStateNone 		  ,
			eButtonStateReleased	  ,
			eButtonStatePresed        ,
			eButtonStateLongPressed   ,
			eButtonStateDoublePressed ,
		}ButtonState;

//	template<typename T>
	class Button
	{
	private:
		uint16_t input;       /* 0 or 1 depending on the input signal */
		uint16_t integrator;  /* Will range from 0 to the specified MAXIMUM */
		uint16_t integratorLongPress;
		ButtonState state;
		ButtonState stateOld;

		ButtonState stateEv;		

		xpcc::Timeout doublePressedTimeout;

		void ButtonDebounce();

	protected:

	public:
		uint8_t counterPressed;

		void ButtonProcess();
		ButtonState ButtonGetState();
		// void ButtonInit(void (*pButton_StateChanged) (uint8_t buttonNumber, ButtonState buttonState));
	};
}
#endif /* BUTTON_H_ */
